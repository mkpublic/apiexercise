# ApiExercise

Projekt demonstrujący wykorzystanie dostępnego publicznie API z serwisu Spotify.

(Do wykonania ćwiczenia niezbędne jest założenie darmowego konta na spotify.com)

## Dokumentacja API Spotify

[Spotify Api Console](https://developer.spotify.com/console/)

## Kolekcja Postman

Przykładowe zapytania do zaimportowania w Postmanie jest w pliku [SDAApiExercise.postman_collection.json](SDAApiExercise.postman_collection.json).

Należy pamiętać o ustawieniu Authorisation Token dla kolekcji (Bearer token).

## Uruchomienie testów

1. Dla uruchomienia testów z poziomu Idea należy w oknie `Run/Debug Configurations` wyedytować konfigurację i dodać zmienną środowiskową `SPOTIFY_API_TOKEN`.

* Najwygodniej uruchomić jeden z testów, jak się nie powiedzie, to wyedytować utworzoną konfigurację
* Wartość tokenu należy odczytać dowolnego zapytania z strony Api Console serwisu Spotify, korzystając z przycisku **GET TOKEN**

2. Dla uruchomienia testów jednostkowych z poziomu linii poleceń (zalecany gitbash):

```
export SPOTIFY_API_TOKEN=<wartość tokenu>
./gradlew test
```

3. Dla uruchomienia testów BDD z poziomu linii poleceń:

```
export SPOTIFY_API_TOKEN=<wartość tokenu>
./gradlew cucumber
```
