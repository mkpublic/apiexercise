Feature: Check artists

  As a user of Spotify I want to search for my favorite artists

  Scenario Outline: My favorite artist is on the list
    Given I provide searched '<artist>' name
    When I submit a request
    Then the request was successful
    And I can find searched "<artist>" in response body

    Examples:
      | artist     |
      | Metallica  |
      | Kazik      |
      | Imelda May |
