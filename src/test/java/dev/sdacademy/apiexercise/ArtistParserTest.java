package dev.sdacademy.apiexercise;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ArtistParserTest {

    private String testData = "{\n" +
            "    \"artists\": {\n" +
            "        \"href\": \"https://api.spotify.com/v1/search?query=Imelda+May&type=artist&offset=0&limit=20\",\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"external_urls\": {\n" +
            "                    \"spotify\": \"https://open.spotify.com/artist/6AnjHMqEAps8VJdHU8RykH\"\n" +
            "                },\n" +
            "                \"followers\": {\n" +
            "                    \"href\": null,\n" +
            "                    \"total\": 125891\n" +
            "                },\n" +
            "                \"genres\": [\n" +
            "                    \"neo-rockabilly\",\n" +
            "                    \"rockabilly\",\n" +
            "                    \"swing\"\n" +
            "                ],\n" +
            "                \"href\": \"https://api.spotify.com/v1/artists/6AnjHMqEAps8VJdHU8RykH\",\n" +
            "                \"id\": \"6AnjHMqEAps8VJdHU8RykH\",\n" +
            "                \"images\": [\n" +
            "                    {\n" +
            "                        \"height\": 640,\n" +
            "                        \"url\": \"https://i.scdn.co/image/5eb2761d01b8b599432ea5665c5d51247165f10b\",\n" +
            "                        \"width\": 640\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"height\": 320,\n" +
            "                        \"url\": \"https://i.scdn.co/image/7de51a9a2d7f92b4376596a6201f5d36f808456e\",\n" +
            "                        \"width\": 320\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"height\": 160,\n" +
            "                        \"url\": \"https://i.scdn.co/image/e0bad952ccba5a9cd6cdae4bf1f95b6a793de009\",\n" +
            "                        \"width\": 160\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"name\": \"Imelda May\",\n" +
            "                \"popularity\": 52,\n" +
            "                \"type\": \"artist\",\n" +
            "                \"uri\": \"spotify:artist:6AnjHMqEAps8VJdHU8RykH\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"external_urls\": {\n" +
            "                    \"spotify\": \"https://open.spotify.com/artist/0U8lpSJXX8oI2vaggS4AmD\"\n" +
            "                },\n" +
            "                \"followers\": {\n" +
            "                    \"href\": null,\n" +
            "                    \"total\": 111\n" +
            "                },\n" +
            "                \"genres\": [],\n" +
            "                \"href\": \"https://api.spotify.com/v1/artists/0U8lpSJXX8oI2vaggS4AmD\",\n" +
            "                \"id\": \"0U8lpSJXX8oI2vaggS4AmD\",\n" +
            "                \"images\": [],\n" +
            "                \"name\": \"The Boogie-Woogie Braves feat. Imelda May\",\n" +
            "                \"popularity\": 2,\n" +
            "                \"type\": \"artist\",\n" +
            "                \"uri\": \"spotify:artist:0U8lpSJXX8oI2vaggS4AmD\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"external_urls\": {\n" +
            "                    \"spotify\": \"https://open.spotify.com/artist/1KMOfoaw2ZsLgcLghRRqEw\"\n" +
            "                },\n" +
            "                \"followers\": {\n" +
            "                    \"href\": null,\n" +
            "                    \"total\": 35\n" +
            "                },\n" +
            "                \"genres\": [],\n" +
            "                \"href\": \"https://api.spotify.com/v1/artists/1KMOfoaw2ZsLgcLghRRqEw\",\n" +
            "                \"id\": \"1KMOfoaw2ZsLgcLghRRqEw\",\n" +
            "                \"images\": [],\n" +
            "                \"name\": \"Darrel Higham and The Enforcers feat. Imelda May\",\n" +
            "                \"popularity\": 0,\n" +
            "                \"type\": \"artist\",\n" +
            "                \"uri\": \"spotify:artist:1KMOfoaw2ZsLgcLghRRqEw\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"limit\": 20,\n" +
            "        \"next\": null,\n" +
            "        \"offset\": 0,\n" +
            "        \"previous\": null,\n" +
            "        \"total\": 3\n" +
            "    }\n" +
            "}";


    @Test
    public void checkTwoArtistsInTheReponseBody() {
        List<SpotifyObject> artists = new ArtistParser().parseResponseBody(testData);

        assertAll( "Artist found on list, on 2 positions",
                () -> {
                    assertEquals(3, artists.size());
                },
                () -> {
                    Artist artist = ArtistParser.convertToArtist(artists.get(0));
                    assertAll("Check first position name and id",
                            () -> assertEquals("Imelda May", artist.getName()),
                            () -> assertEquals("6AnjHMqEAps8VJdHU8RykH", artist.getId())
                    );
                },
                () -> {
                    Artist artist = ArtistParser.convertToArtist(artists.get(1));
                    assertAll("Check second position name and id",
                            () -> assertEquals("The Boogie-Woogie Braves feat. Imelda May", artist.getName()),
                            () -> assertEquals("0U8lpSJXX8oI2vaggS4AmD", artist.getId())
                    );
                },
                () -> {
                    Artist artist = ArtistParser.convertToArtist(artists.get(2));
                    assertAll("Check third position name and id",
                            () -> assertEquals("Darrel Higham and The Enforcers feat. Imelda May", artist.getName()),
                            () -> assertEquals("1KMOfoaw2ZsLgcLghRRqEw", artist.getId())
                    );
                }
        );
    }

}