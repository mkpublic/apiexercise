package dev.sdacademy.apiexercise;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class RestCallTest {

    @Test
    public void testSendRequest() {
        ArtistRestCall restCall = new ArtistRestCall(new ArtistParser());
        restCall.setArtistName("Kazik");
        assertTrue(restCall.sendRequest());
    }

}