package stepdefinitions;

import dev.sdacademy.apiexercise.Artist;
import dev.sdacademy.apiexercise.ArtistParser;
import dev.sdacademy.apiexercise.ArtistRestCall;
import dev.sdacademy.apiexercise.RestCall;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

public class ArtistSteps {

    RestCall currentRestCall;

    @Given("I provide searched {string} name")
    public void iProvideSearchedArtistName(String artist) {
        currentRestCall = new ArtistRestCall(new ArtistParser());
        ((ArtistRestCall)currentRestCall).setArtistName(artist);
    }

    @When("I submit a request")
    public void iSubmitARequest() {
        currentRestCall.sendRequest();
    }

    @Then("the request was successful")
    public void theRequestWasSuccessful() {
        assertEquals(200, currentRestCall.getResponseStatus());
    }

    @Then("I can find searched {string} in response body")
    public void iCanFindSearchedArtistInResponseBody(String artist) {
        List<Artist> artistList = ((ArtistRestCall)currentRestCall).getArtistList();
        for (Artist artistElement: artistList) {
            assertTrue(artistElement.getName().contains(artist));
        }
    }
}
