package dev.sdacademy.apiexercise;

import java.util.List;

public interface Parser {

    List<SpotifyObject> parseResponseBody(String responseBody);

}
