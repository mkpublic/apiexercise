package dev.sdacademy.apiexercise;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ArtistParser implements Parser {
    @Override
    public List<SpotifyObject> parseResponseBody(String responseBody) {
        List<SpotifyObject> artistsList = new ArrayList<>();

        JSONObject jsonObject = new JSONObject(responseBody).getJSONObject("artists");
        JSONArray artists = jsonObject.getJSONArray("items");

        for (int i = 0; i < artists.length(); i++) {
            JSONObject artist = artists.getJSONObject(i);
            artistsList.add(new Artist(artist.getString("name"), artist.getString("id")));
        }

        return artistsList;
    }

    public static Artist convertToArtist(SpotifyObject spotifyObject) {
        if (spotifyObject instanceof Artist) {
            return (Artist)spotifyObject;
        } else {
            return null;
        }
    }
}
