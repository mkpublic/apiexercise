package dev.sdacademy.apiexercise;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.List;

public abstract class RestCall {

    private int responseStatus;
    private String responseStatusText;
    private Parser responseParser;
    private List<SpotifyObject> responseObjects;
    protected String baseUrl;

    public RestCall(Parser responseParser) {
        this.responseParser = responseParser;
        baseUrl = "https://api.spotify.com/v1/";
    }

    public boolean sendRequest() {
        Unirest.setTimeouts(0, 0);
        try {
            HttpResponse<String> response = getResponse();
            responseStatusText = response.getStatusText();
            responseStatus = response.getStatus();
            var body = response.getBody();
            if (responseStatus != 200) {
                return false;
            }
            responseObjects = responseParser.parseResponseBody(body);
        } catch (UnirestException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public int getResponseStatus() {
        return responseStatus;
    }

    public String getResponseStatusText() {
        return responseStatusText;
    }

    public List<SpotifyObject> getResponseObjects() {
        return responseObjects;
    }

    protected abstract HttpResponse<String> getResponse() throws UnirestException;

    protected String getTokenValue() {
        String token = System.getenv("SPOTIFY_API_TOKEN");
        return token;
    }
}
