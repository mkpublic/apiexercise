package dev.sdacademy.apiexercise;

public class Artist extends SpotifyObject {
    private String name;
    private String id;

    public Artist(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
