package dev.sdacademy.apiexercise;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ArtistRestCall extends RestCall {

    private String artistName;

    public ArtistRestCall(Parser responseParser) {
        super(responseParser);
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public List<Artist> getArtistList() {
        List<Artist> artistList = new ArrayList<>();
        for (SpotifyObject spotifyObject: getResponseObjects()) {
            artistList.add(ArtistParser.convertToArtist(spotifyObject));
        }
        return artistList;
    }

    @Override
    protected HttpResponse<String> getResponse() throws UnirestException {
        try {
            String request = String.format("%ssearch?q=%s&type=artist",
                    baseUrl,
                    URLEncoder.encode(artistName, StandardCharsets.UTF_8.toString()));
            return Unirest.get(request)
                    .header("Authorization",
                            "Bearer " + getTokenValue())
                    .asString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

}
